package com.cs.apac.hackathon.client.chatbot.jarvis.remote;

import com.cs.apac.hackathon.client.chatbot.jarvis.dto.GraphResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class FacebookGraphService {

    @Autowired
    @Qualifier("facebookGraphRestTemplate")
    RestTemplate facebookGraphRestTemplate;

    public String getFbId(String senderId) {
        GraphResponse response = facebookGraphRestTemplate.getForObject("https://graph.facebook.com/v2.11/"
                + senderId + "/ids_for_apps?page=279380106055201&access_token=EAADZBGDZC6jiEBAMeNiih28DKABvkZCYytDJVfZA7VpW7hRiIRZB8GZCIolfLx9xhAy0vlvMlTlehW22rkDzOQvwI8FUwe3DKcPixGhmkhrOSoJdyA1lm1ZARdB85qpf2G81i7F7aOpI5JDRgUI0Sqnod7hQKFZAZBNfpQfPM9AX0jwZDZD", GraphResponse.class);
        if (response.getData().isEmpty()) {
            return "NOT_LINKED";
        }

        String id = response.getData().get(0).getId();
        System.out.println(id);
        return id;
    }
}
