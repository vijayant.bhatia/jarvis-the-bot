package com.cs.apac.hackathon.client.chatbot.jarvis.service;

import ai.api.AIConfiguration;
import ai.api.AIDataService;
import ai.api.model.AIRequest;
import ai.api.model.AIResponse;
import com.cs.apac.hackathon.client.chatbot.jarvis.dto.Account;
import com.cs.apac.hackathon.client.chatbot.jarvis.dto.Branch;
import com.cs.apac.hackathon.client.chatbot.jarvis.dto.Transaction;
import com.cs.apac.hackathon.client.chatbot.jarvis.model.ActionResponse;
import com.cs.apac.hackathon.client.chatbot.jarvis.model.SenderSession;
import com.cs.apac.hackathon.client.chatbot.jarvis.remote.ClientChatbotService;
import com.cs.apac.hackathon.client.chatbot.jarvis.remote.FacebookGraphService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.Date;
import java.util.List;

@Service
public class NLPService {

    @Autowired
    FacebookGraphService facebookGraphService;

    @Autowired
    ClientChatbotService clientChatbotService;

    @Autowired
    RestTemplate appServiceRestTemplate;

    private static final Logger LOGGER = LoggerFactory.getLogger(NLPService.class);


    /*
    1.  Check for session using the service call to BFMService  (Table contains senderID, lastLoginTS, authenticatedFlag, securePin, updateTS)
    2.  If not Authenticate ask for secure pin and check
    3.  If authenticated then show message and call services according to the action
    4.  for account service of multiple accounts send the acct. id along with the request to check the balance
    */

    public ActionResponse getText(String senderId, String messageText) {
        //Fetch ASID using PSID
        String socialId = fetchId(senderId);
        ActionResponse actionResponse = new ActionResponse();
        if (!isCustomerLinkedWithBot(socialId, actionResponse)) return actionResponse;
        //Check authentication, if not do the authentication
        if (!authenticate(socialId, actionResponse)) return actionResponse;

        SenderSession senderSession = clientChatbotService.getSenderSession(socialId);
        if ("authenticationRequired".equals(actionResponse.getAction())) {
            if (!messageText.equals(senderSession.getSecurePin())) {
                actionResponse.setResponse("Invalid secure pin please enter again");
                return actionResponse;
            } else {
                messageText = "Hi";
            }
        }
        LOGGER.info("Will create AIConfiguration");

        AIConfiguration configuration = new AIConfiguration("ffa8414cc30c4d779454441363878a00");
        AIDataService dataService = new AIDataService(configuration);

        String line;
        try {
            AIRequest request = new AIRequest(messageText);
            AIResponse response = dataService.request(request);
            if (response.getStatus().getCode() == 200) {
                LOGGER.info("Speech: {}", response.getResult().getFulfillment().getSpeech());
                line = response.getResult().getFulfillment().getSpeech();
                actionResponse.setAction(response.getResult().getAction());
                LOGGER.info("action >>>>>>>>>>>>> {}", actionResponse.getAction());
                actionResponse.setResponse(line);
                actionResponse.setSenderId(socialId);
                if ("input.unknown".equals(actionResponse.getResponse())) {
                    actionResponse.setServiceCallFlag(false);
                    actionResponse.setResponse(line);
                } else {
                    actionResponse.setServiceCallFlag(true);
                }
            } else {
                String errorDetails = response.getStatus().getErrorDetails();
                LOGGER.info("ErrorDetails: {}", errorDetails);
                line = errorDetails;
                actionResponse.setAction(response.getResult().getAction());
                actionResponse.setResponse(line);
                actionResponse.setSenderId(socialId);
            }
            clientChatbotService.updateSenderSession(senderSession);

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return actionResponse;
    }

    private boolean isCustomerLinkedWithBot(String socialId, ActionResponse actionResponse) {
        SenderSession senderSession = clientChatbotService.getSenderSession(socialId);
        try {
            if (null == senderSession) {
                actionResponse.setAction("customerNotLinked");
                actionResponse.setResponse("Please link your facebook account with Credit Suisse account, using bank's site");
                actionResponse.setSenderId(socialId);
                actionResponse.setServiceCallFlag(false);
                return false;
            } else {
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            actionResponse.setResponse("Something went wrong, our customer care representative will call you");
            return false;
        }
    }

    private Object fetchObject(String jsonString, Class targetType) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(jsonString, targetType);
    }

    private boolean authenticate(String socialId, ActionResponse actionResponse) {
        SenderSession senderSession = clientChatbotService.getSenderSession(socialId);
        LOGGER.info("Inside Authentication getSenderSession: {} >>{}", senderSession, socialId);
        if (null == senderSession) {
            clientChatbotService.saveSenderSession(socialId);
            return false;
        }

        if (senderSession.isSessionStarted()) {
            if (senderSession.isAuthRequired()) {
                LOGGER.info("Inside Authentication isAuthRequired = true and Session started");
                actionResponse.setAction("authenticationRequired");
            }
            return true;
        }
        long i = new Date().getTime() - senderSession.getUpdateTS().getTime();
        if (senderSession.isAuthenticatedFlag() && i < 50000) {
            LOGGER.info("Inside Authentication Authenticated = true & under time limit");
            return true;
        } else {
            LOGGER.info("Inside Authentication else block");
            actionResponse.setAction("authenticationRequired");
            actionResponse.setResponse("Please enter secure pin for authentication");
            actionResponse.setSenderId(socialId);
            actionResponse.setServiceCallFlag(false);
            clientChatbotService.updateSenderSessionForPin(senderSession);
            return false;
        }
    }

    public String fetchId(String senderId) {
        return facebookGraphService.getFbId(senderId);
    }

    public String hitServices(ActionResponse actionResponse) {

        switch (actionResponse.getAction()) {
            case "input.welcome":
                return actionResponse.getResponse()+"\nWhat can I help you with";
            case "client.details.check":
                Account accountDetails = clientChatbotService.getAccount(actionResponse.getSenderId());
                if (null != accountDetails) {
                    return "Your Account Details are as follows: 1.Account Number - " + accountDetails.getId().toString() + " 2.Account Balance - Rs." + accountDetails.getAccountBalance() + " 3.Branch Name - " + accountDetails.getBranch().getName() + " 4.IFSC Code - " + accountDetails.getBranch().getIFSCCode();
                } else {
                    return "Sorry! We couldn't find any account details associated with you";

                }
            case "userDetails":
                break;
            case "account.spending.check":
            case "account.earning.check":

                List<Transaction> transactions = clientChatbotService.getTransactions(actionResponse.getSenderId());
                if (null != transactions) {
                    StringBuilder sb = new StringBuilder("Your details of last 5 transactions are as follows:\n");
                    for (int i = 0; i < transactions.size(); i++) {
                        LOGGER.info(transactions.get(i).toString());
                        Transaction current = transactions.get(i);
                        sb.append(i+1).append(". ").append(transactions.get(i).getCategory()).append(" ")
                                .append(current.getTransactionAmount()).append(" ").append(current.getTransactionDate())
                                .append("\n");
                    }
                    return sb.toString();
                } else {
                    return "Sorry! We couldn't find any transaction details associated with you";
                }
            case "branch.details.get":
                Branch branch = clientChatbotService.getBranch(actionResponse.getSenderId());
                if (null != branch) {
                    StringBuilder sb =new StringBuilder("Your Bank Branch Details are - \n Branch Name: ")
                            .append( branch.getName()).append(" IFSC Code: ").append(branch.getIFSCCode())
                            .append(" Branch Address: ").append(branch.getAddress());
                    return sb.toString();
                } else {
                    return "Sorry! We couldn't find Branch Details associated with your account!";

                }

            case "account.balance.check":
                Account accountDetails1 = clientChatbotService.getAccount(actionResponse.getSenderId());
                if (null != accountDetails1) {
                    return "Your Account Balance is - " + " Rs." + accountDetails1.getAccountBalance();
                } else {
                    return "Sorry! We couldn't find any account balance associated with you";

                }
            case "followup.action":
                return "What else would you like me to do?";
            case "smalltalk.confirmation.no":
            case "goodbye.action":
                LOGGER.info("Good bye Action");
                SenderSession senderSessionRequest = new SenderSession();
                senderSessionRequest.setSenderID(actionResponse.getSenderId());
                clientChatbotService.logoutSenderSession(senderSessionRequest);
                return "";
            default:
                break;

        }

        return "";
    }

}
