package com.cs.apac.hackathon.client.chatbot.jarvis.config;

import com.cs.apac.hackathon.client.chatbot.jarvis.interceptor.ServiceHeaderInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class RemoteServiceConfig {

    @Bean
    public RestTemplate facebookGraphRestTemplate() {
        return new RestTemplate();
    }

    @Bean
    public RestTemplate servicesRestTemplate() {
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getInterceptors().add(new ServiceHeaderInterceptor());
        return restTemplate;
    }

    @Bean
    public RestTemplate appServiceRestTemplate() {
        return new RestTemplate();
    }
}
