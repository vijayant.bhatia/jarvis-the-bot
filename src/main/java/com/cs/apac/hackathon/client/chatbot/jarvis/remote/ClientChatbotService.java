package com.cs.apac.hackathon.client.chatbot.jarvis.remote;

import com.cs.apac.hackathon.client.chatbot.jarvis.dto.Account;
import com.cs.apac.hackathon.client.chatbot.jarvis.dto.Branch;
import com.cs.apac.hackathon.client.chatbot.jarvis.dto.OfferDetails;
import com.cs.apac.hackathon.client.chatbot.jarvis.dto.Transaction;
import com.cs.apac.hackathon.client.chatbot.jarvis.model.SenderSession;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.Collections;
import java.util.Date;
import java.util.List;

@Service
public class ClientChatbotService {

    @Autowired
    RestTemplate servicesRestTemplate;

    @Value("${remote.banksvc.endpoint}")
    String baseUrl;

    public Account getAccount(String ASID) {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers.setContentType(MediaType.TEXT_HTML);
        HttpEntity<String> entity = new HttpEntity("parameters", headers);
        String url = baseUrl + "/account/details/" + ASID;
        Account accountDetails = servicesRestTemplate.getForObject(url, Account.class);
        return accountDetails;
    }

    public List<Transaction> getTransactions(String ASID) {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers.setContentType(MediaType.TEXT_HTML);
        HttpEntity<String> entity = new HttpEntity("parameters", headers);
        String url = baseUrl + "/account/transactions/last/" + ASID;
        ResponseEntity<List<Transaction>> response = servicesRestTemplate.exchange(url, HttpMethod.GET,
                null, new ParameterizedTypeReference<List<Transaction>>() {
                });
        return response.getBody();
    }


    public Branch getBranch(String ASID) {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers.setContentType(MediaType.TEXT_HTML);
        HttpEntity<String> entity = new HttpEntity("parameters", headers);
        String url = baseUrl + "/account/branch/" + ASID;
        Branch branch = servicesRestTemplate.getForObject(url, Branch.class);
        return branch;
    }


    public OfferDetails getOffer(String ASID, String category) {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers.setContentType(MediaType.TEXT_HTML);
        HttpEntity<String> entity = new HttpEntity("parameters", headers);
        String url = baseUrl + "/offer/" + ASID + "/" + category;
        OfferDetails offerDetails = servicesRestTemplate.getForObject(url, OfferDetails.class);
        return offerDetails;
    }


    public SenderSession getSenderSession(String senderId) {
        SenderSession senderSession = servicesRestTemplate.getForObject(baseUrl + "sendersession/" + senderId, SenderSession.class);
        return senderSession;
    }

    public SenderSession saveSenderSession(String senderId) {
        SenderSession senderSessionRequest = new SenderSession();
        senderSessionRequest.setAuthenticatedFlag(true);
        senderSessionRequest.setLastLoginTS(new Timestamp(new Date().getTime()));
        SenderSession senderSession = servicesRestTemplate.postForObject(baseUrl + "sendersession", senderSessionRequest, SenderSession.class);
        return senderSession;
    }

    public SenderSession updateSenderSession(SenderSession senderSessionRequest) {
        senderSessionRequest.setSessionStarted(false);
        senderSessionRequest.setAuthenticatedFlag(true);
        senderSessionRequest.setUpdateTS(new Timestamp(new Date().getTime()));
        senderSessionRequest.setAuthRequired(false);
        SenderSession senderSession = servicesRestTemplate.postForObject(baseUrl + "sendersession", senderSessionRequest, SenderSession.class);
        return senderSession;
    }

    public SenderSession updateSenderSessionForPin(SenderSession senderSessionRequest) {
        senderSessionRequest.setSessionStarted(true);
        senderSessionRequest.setAuthenticatedFlag(true);
        senderSessionRequest.setUpdateTS(new Timestamp(new Date().getTime()));
        senderSessionRequest.setAuthRequired(true);
        SenderSession senderSession = servicesRestTemplate.postForObject(baseUrl + "sendersession", senderSessionRequest, SenderSession.class);
        return senderSession;
    }

    public SenderSession logoutSenderSession(SenderSession senderSessionRequest) {
        senderSessionRequest = getSenderSession(senderSessionRequest.getSenderID());
        senderSessionRequest.setSessionStarted(false);
        senderSessionRequest.setAuthenticatedFlag(false);
        senderSessionRequest.setUpdateTS(new Timestamp(new Date().getTime()));
        SenderSession senderSession = servicesRestTemplate.postForObject(baseUrl + "sendersession", senderSessionRequest, SenderSession.class);
        return senderSession;
    }

    public <T> List<T> fetchList(String jsonString, Class targetType) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        JavaType type = mapper.getTypeFactory().constructCollectionType(List.class, targetType);
        return mapper.readValue(jsonString, type);
    }

    public void saveSenderSession(SenderSession senderSession) {
        senderSession.setAuthenticatedFlag(false);
        senderSession.setSessionStarted(false);
        senderSession.setLastLoginTS(new Timestamp(new Date().getTime()));
        senderSession.setUpdateTS(new Timestamp(new Date().getTime()));
        servicesRestTemplate.postForObject(baseUrl + "sendersession", senderSession, SenderSession.class);
    }


}
